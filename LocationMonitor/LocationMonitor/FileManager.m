//
//  FileManager.m
//  LocationMonitor
//
//  Created by Austin Kelley on 2/10/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import "FileManager.h"

NSString * const kFileManagerFilenamePrefix = @"FileManager_File_";

@interface FileManager ()

@property (nonatomic) NSInteger fileNumber;

@end

@implementation FileManager

+(FileManager*)sharedInstance
{
    static dispatch_once_t p = 0;
    __strong static id _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(instancetype) init {
    if (self = [super init])
    {
        self.fileNumber = 0;
        NSDirectoryEnumerator *homeContents = [[NSFileManager defaultManager] enumeratorAtPath:self.homeDirectory];
        NSString *filename;
        while ( (filename = [homeContents nextObject])) {
            if ([filename hasPrefix:kFileManagerFilenamePrefix]) {
                NSInteger filenumber = [filename substringFromIndex:kFileManagerFilenamePrefix.length].integerValue;
                if (filenumber > self.fileNumber)
                    self.fileNumber = filenumber;
            }
        }
//        [self deleteFileAtPath:[self.homeDirectory stringByAppendingPathComponent:@"myfile.txt"]];
    }
    return self;
}

-(NSString*) homeDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}

-(NSString*) currentFilename {
    return [NSString stringWithFormat:@"%@%ld", kFileManagerFilenamePrefix, (long)self.fileNumber];
}

-(NSString*) currentFilePath {
    return [self.homeDirectory stringByAppendingPathComponent:self.currentFilename];
}

-(void) setFileNumber:(NSInteger)fileNumber {
    _fileNumber = fileNumber;
    BOOL isDir = NO;
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.currentFilePath isDirectory:&isDir]) {
        [[NSFileManager defaultManager] createFileAtPath:self.currentFilePath contents:[NSData data] attributes:nil];
    }
}

-(void) startNewFile {
    self.fileNumber++;
}

-(NSArray*) filesFromDirectory:(NSString*)dirPath {
    NSString *path = dirPath ? dirPath : self.homeDirectory;
    
    NSMutableArray *ret = [NSMutableArray array];
    
    NSDirectoryEnumerator *homeContents = [[NSFileManager defaultManager] enumeratorAtPath:path];
    NSString *filename;
    while ( (filename = [homeContents nextObject])) {
        [ret addObject:filename];
    }
    return [ret copy];
}

-(void) addDictionary:(NSDictionary*)dictionary toJSONArrayInFile:(NSString*)filepath {
    NSString *path = filepath ? filepath : self.currentFilePath;
    NSString *contents = [self contentsOfFile:path];
    id contentsJSONArray = [NSJSONSerialization JSONObjectWithData:[contents dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    if (!contentsJSONArray) {
        NSArray *array = [NSArray arrayWithObject:dictionary];
        
        NSString *newContents = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:array options:0 error:nil]  encoding:NSUTF8StringEncoding];
        [self writeString:newContents toFile:path];
    }
    
    if ([contentsJSONArray isKindOfClass:[NSArray class]]) {
        NSMutableArray *mutable = [NSMutableArray arrayWithArray:(NSArray*)contentsJSONArray];
        [mutable addObject:dictionary];
        
        NSString *newContents = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:mutable options:0 error:nil]  encoding:NSUTF8StringEncoding];
        [self writeString:newContents toFile:path];
    }
}

-(void) writeString:(NSString*)aString toFile:(NSString*)filepath {
    NSString *path = filepath ? filepath : self.currentFilePath;
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
    [fileHandle writeData:[aString dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandle closeFile];
}

-(void) appendString:(NSString*)aString toFile:(NSString*)filepath {
    NSString *path = filepath ? filepath : self.currentFilePath;
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
    [fileHandle seekToEndOfFile];
    [fileHandle writeData:[aString dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandle closeFile];
}

-(NSString*) contentsOfFile:(NSString*)filepath {
    NSString *path = filepath ? filepath : self.currentFilePath;
    return [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
}

-(void) clearFile:(NSString*)filepath {
    [self writeString:@"" toFile:filepath];
}

-(void) deleteFileAtPath:(NSString*)filepath {
    NSString *path = filepath ? filepath : self.currentFilePath;
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

-(void) clearHomeDirectory {
    NSDirectoryEnumerator *homeContents = [[NSFileManager defaultManager] enumeratorAtPath:self.homeDirectory];
    NSString *filename;
    while ( (filename = [homeContents nextObject])) {
        NSString *filepath = [self.homeDirectory stringByAppendingPathComponent:filename];
        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
    }
}

@end

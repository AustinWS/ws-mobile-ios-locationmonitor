//
//  AppDelegate.h
//  LocationMonitor
//
//  Created by Austin Kelley on 2/7/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


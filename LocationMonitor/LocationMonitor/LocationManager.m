//
//  LocationManager.m
//  LocationMonitor
//
//  Created by Austin Kelley on 2/7/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import "LocationManager.h"

#import <UIKit/UIKit.h>

#import "FileManager.h"
#import "DeviceManager.h"
#import "NotificationManager.h"

#import "UserDefaultsProperties.h"

NSString * const kLocationManagerLocationsKey = @"locations";
NSString * const kLocationManagerIsUpdatingLocationKey = @"isUpdatingLocation";
NSString * const kLocationManagerIsMonitoringLocationKey = @"isMonitoringLocation";


@interface LocationManager () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation LocationManager

+ (LocationManager*)sharedInstance
{
    static dispatch_once_t p = 0;
    __strong static id _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(instancetype) init {
    
    if (self = [super init]) {
        
        if (!self.locationManager) {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
            self.locationManager.allowsBackgroundLocationUpdates = YES;
            
            self.locationManager.distanceFilter = 500;
            self.locationManager.pausesLocationUpdatesAutomatically = YES;
            self.locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        }
        
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            [self.locationManager requestAlwaysAuthorization];
        }
    }
    return self;
}

-(CLLocationAccuracy) accuracy {
    return self.locationManager.desiredAccuracy;
}

-(NSString*) accuracyString {
    if (self.locationManager.desiredAccuracy == kCLLocationAccuracyBest)
        return @"Best";
    else if (self.locationManager.desiredAccuracy == kCLLocationAccuracyNearestTenMeters)
        return @"10 m";
    else if (self.locationManager.desiredAccuracy == kCLLocationAccuracyHundredMeters)
        return @"100 m";
    else if (self.locationManager.desiredAccuracy == kCLLocationAccuracyKilometer)
        return @"1 km";
    else if (self.locationManager.desiredAccuracy == kCLLocationAccuracyThreeKilometers)
        return @"3 km";
    else
        return @(self.locationManager.desiredAccuracy).stringValue;
}

-(void) setAccuracy:(CLLocationAccuracy)accuracy {
    self.locationManager.desiredAccuracy = accuracy;
}

-(CLActivityType) activityType {
    return self.locationManager.activityType;
}

-(NSString*) activityTypeString {
    switch (self.locationManager.activityType) {
        case CLActivityTypeOther:
            return @"Other";
        case CLActivityTypeFitness:
            return @"Fitness";
        case CLActivityTypeOtherNavigation:
            return @"Other Nav";
        case CLActivityTypeAutomotiveNavigation:
            return @"Auto Nav";
        default:
            return @"Unrecognized";
    }
}

-(void) setActivityType:(CLActivityType)type {
    self.locationManager.activityType = type;
}

-(void) startUpdatingLocation {
    
    [self.locationManager startUpdatingLocation];
    self.isUpdatingLocation = YES;
    [[FileManager sharedInstance] startNewFile];
}

-(void) stopUpdatingLocation {
    
    [self.locationManager stopUpdatingLocation];
    self.isUpdatingLocation = NO;
}

-(void) startMonitoringLocation {
    
    [self.locationManager startMonitoringSignificantLocationChanges];
    self.isMonitoringLocation = YES;
    [[FileManager sharedInstance] startNewFile];
}

-(void) stopMonitoringLocation {
    
    [self.locationManager stopMonitoringSignificantLocationChanges];
    self.isMonitoringLocation = NO;
}



/*
 *  locationManager:didUpdateLocations:
 *
 *  Discussion:
 *    Invoked when new locations are available.  Required for delivery of
 *    deferred locations.  If implemented, updates will
 *    not be delivered to locationManager:didUpdateToLocation:fromLocation:
 *
 *    locations is an array of CLLocation objects in chronological order.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    self.locations = locations;
    
    NSString *formatted = [self formattedStringFromLocations:locations];
    
    NSLog(@"Received location update: %@", formatted);
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        
        CLLocation *first = locations.firstObject;
        
        NSString *fileContents = [[FileManager sharedInstance] contentsOfFile:[[NSBundle mainBundle] pathForResource:@"places_json" ofType:@"js"]];
        NSArray *itemsToCheck = [NSJSONSerialization JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        NSMutableString *names = [NSMutableString string];
        
        for (NSDictionary *item in itemsToCheck) {
            NSNumber *lat = item[@"lat"];
            NSNumber *lon = item[@"long"];
            CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat.doubleValue longitude:lon.doubleValue];
            
            NSNumber *distance = item[@"distance"];
            
            if ([first distanceFromLocation:loc] < distance.doubleValue) {
                if (names.length)
                    [names appendString:@", "];
                [names appendString:item[@"name"]];
            }
        }
        
        NSString *body = @"It was pretty cool.";
        
        if (names.length)
        {
            body = [body stringByAppendingFormat:@" You're close to: %@", names];
        }
        
        [[NotificationManager sharedInstance] fireLocalNotificationWithTitle:@"Your location updated!" body:body];
    }
    
    for (CLLocation *loc in locations) {
        [[FileManager sharedInstance] addDictionary:[self dictionaryFromLocation:loc] toJSONArrayInFile:nil];
    }
    
}

-(NSDictionary*) dictionaryFromLocation:(CLLocation*)loc {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"latitude"] = @(loc.coordinate.latitude);
    dict[@"longitude"] = @(loc.coordinate.longitude);
    dict[@"h_accur"] = @(loc.horizontalAccuracy);
    dict[@"v_accur"] = @(loc.verticalAccuracy);
    dict[@"speed"] = @(loc.speed);
    dict[@"course"] = @(loc.course);
    dict[@"time"] = loc.timestamp.description; //TODO: date format this to local timezone
    dict[@"timestamp"] = @([loc.timestamp timeIntervalSince1970]);
    dict[@"settings"] = @{@"accuracy" : self.accuracyString,
                          @"activity" : self.activityTypeString};
    dict[@"device"] = @{@"model" : [DeviceManager sharedInstance].prettyDeviceName,
                        @"version" : [[UIDevice currentDevice] systemVersion]};
    dict[@"battery"] = @{@"percentage" : [DeviceManager sharedInstance].batteryPercentageString,
                        @"charge_status" : [DeviceManager sharedInstance].batteryChargeStatusString};
    dict[@"name"] = [[NSUserDefaults standardUserDefaults] stringForKey:kSettingsViewControllerNameDefaultsKey];
    return [dict copy];
}

-(NSString*) formattedStringFromLocations:(NSArray*)locations {
    
    NSMutableString *logline = [NSMutableString stringWithFormat:@""];
    
    for (CLLocation *loc in locations)
    {
        [logline appendString:@"{\n"];
        [logline appendFormat:@"\t\"Latitude\" : %f,\n", loc.coordinate.latitude];
        [logline appendFormat:@"\t\"Longitude\" : %f,\n", loc.coordinate.longitude];
        [logline appendFormat:@"\t\"Horiz Accuracy\" : %f,\n", loc.horizontalAccuracy];
        [logline appendFormat:@"\t\"Speed\" : %f,\n", loc.speed];
        [logline appendFormat:@"\t\"Course\" : %f,\n", loc.course];
        [logline appendFormat:@"\t\"Timestamp\" : \"%@\",\n", loc.timestamp.description];
        [logline appendString:@"},\n"];
    }
    
    return logline;
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(nonnull NSError *)error
{
    
}

/*
 *  locationManager:didEnterRegion:
 *
 *  Discussion:
 *    Invoked when the user enters a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 */
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    
}

/*
 *  locationManager:didExitRegion:
 *
 *  Discussion:
 *    Invoked when the user exits a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 */
- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    
}

@end

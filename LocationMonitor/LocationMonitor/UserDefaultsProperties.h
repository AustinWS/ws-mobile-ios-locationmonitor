//
//  UserDefaultsProperties.h
//  LocationMonitor
//
//  Created by Austin Kelley on 2/14/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kSettingsViewControllerNameDefaultsKey;

@interface UserDefaultsProperties : NSObject

@end

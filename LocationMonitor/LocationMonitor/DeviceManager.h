//
//  DeviceManager.h
//  LocationMonitor
//
//  Created by Austin Kelley on 2/13/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceManager : NSObject

+(DeviceManager*)sharedInstance;

@property (nonatomic, readonly) NSString *deviceName;
@property (nonatomic, readonly) NSString *prettyDeviceName;

@property (nonatomic, readonly) NSString *batteryPercentageString;
@property (nonatomic, readonly) NSString *batteryChargeStatusString;


@end

//
//  SettingsViewController.m
//  LocationMonitor
//
//  Created by Austin Kelley on 2/10/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import "SettingsViewController.h"

#import "LocationManager.h"
#import "UserDefaultsProperties.h"


@interface SettingsViewController()

@property (nonatomic, strong) IBOutlet UITextField *nameTextField;

@property (nonatomic, strong) IBOutlet UISegmentedControl *accuracyControl;
@property (nonatomic, strong) IBOutlet UISegmentedControl *activityControl;

@end


@implementation SettingsViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    
    if ([LocationManager sharedInstance].accuracy == kCLLocationAccuracyBest) {
        self.accuracyControl.selectedSegmentIndex = 0;
    } else if ([LocationManager sharedInstance].accuracy == kCLLocationAccuracyNearestTenMeters) {
        self.accuracyControl.selectedSegmentIndex = 1;
    } else if ([LocationManager sharedInstance].accuracy == kCLLocationAccuracyHundredMeters) {
        self.accuracyControl.selectedSegmentIndex = 2;
    } else if ([LocationManager sharedInstance].accuracy == kCLLocationAccuracyKilometer) {
        self.accuracyControl.selectedSegmentIndex = 3;
    } else if ([LocationManager sharedInstance].accuracy == kCLLocationAccuracyThreeKilometers) {
        self.accuracyControl.selectedSegmentIndex = 4;
    }
    
    switch ([LocationManager sharedInstance].activityType) {
        case CLActivityTypeOther:
            self.activityControl.selectedSegmentIndex = 0;
            break;
        case CLActivityTypeAutomotiveNavigation:
            self.activityControl.selectedSegmentIndex = 1;
            break;
        case CLActivityTypeFitness:
            self.activityControl.selectedSegmentIndex = 2;
            break;
        case CLActivityTypeOtherNavigation:
            self.activityControl.selectedSegmentIndex = 3;
            break;
        default:
            break;
    }
    
    self.nameTextField.text = [[NSUserDefaults standardUserDefaults] stringForKey:kSettingsViewControllerNameDefaultsKey];
}

-(IBAction) done:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction) valueChanged:(id)sender {
    if (sender == self.accuracyControl) {
        switch (self.accuracyControl.selectedSegmentIndex) {
            case 0:
                [[LocationManager sharedInstance] setAccuracy:kCLLocationAccuracyBest];
                break;
            case 1:
                [[LocationManager sharedInstance] setAccuracy:kCLLocationAccuracyNearestTenMeters];
                break;
            case 2:
                [[LocationManager sharedInstance] setAccuracy:kCLLocationAccuracyHundredMeters];
                break;
            case 3:
                [[LocationManager sharedInstance] setAccuracy:kCLLocationAccuracyKilometer];
                break;
            case 4:
                [[LocationManager sharedInstance] setAccuracy:kCLLocationAccuracyThreeKilometers];
                break;
            default:
                break;
        }
    } else if (sender == self.activityControl) {
        switch (self.activityControl.selectedSegmentIndex) {
            case 0:
                [[LocationManager sharedInstance] setActivityType:CLActivityTypeOther];
                break;
            case 1:
                [[LocationManager sharedInstance] setActivityType:CLActivityTypeAutomotiveNavigation];
                break;
            case 2:
                [[LocationManager sharedInstance] setActivityType:CLActivityTypeFitness];
                break;
            case 3:
                [[LocationManager sharedInstance] setActivityType:CLActivityTypeOtherNavigation];
                break;
            default:
                break;
        }
    } else if (sender == self.nameTextField) {
        [[NSUserDefaults standardUserDefaults] setObject:self.nameTextField.text forKey:kSettingsViewControllerNameDefaultsKey];
    }
    
}

@end

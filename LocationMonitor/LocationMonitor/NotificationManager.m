//
//  NotificationManager.m
//  LocationMonitor
//
//  Created by Austin Kelley on 2/7/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import "NotificationManager.h"

#import <UserNotifications/UserNotifications.h>

@implementation NotificationManager

+ (NotificationManager*)sharedInstance
{
    static dispatch_once_t p = 0;
    __strong static id _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(instancetype) init {
    if (self = [super init]) {
        
        UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound;
        
        UNUserNotificationCenter *unc = [UNUserNotificationCenter currentNotificationCenter];
        [unc requestAuthorizationWithOptions:options
                           completionHandler:^(BOOL granted, NSError * _Nullable error) {
                               if (!granted) {
                                   NSLog(@"Something went wrong");
                               }
                           }];
    }
    return self;
}

-(void) fireLocalNotificationWithTitle:(NSString*)title body:(NSString*)body
{
    return;
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    content.title = title;
    content.body = body;
    content.sound = [UNNotificationSound defaultSound];
    
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:10 repeats:NO];
    
    NSString *identifier = @"UYLLocalNotification";
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier
                                                                          content:content trigger:trigger];
    
    UNUserNotificationCenter *unc = [UNUserNotificationCenter currentNotificationCenter];
    [unc addNotificationRequest:request
          withCompletionHandler:^(NSError * _Nullable error) {
              if (error != nil) {
                  NSLog(@"Something went wrong: %@",error);
              }
          }];
}

@end

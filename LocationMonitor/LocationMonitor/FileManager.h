//
//  FileManager.h
//  LocationMonitor
//
//  Created by Austin Kelley on 2/10/17.
//  Copyright © 2017 Intersections. All rights reserved.
//


#import <Foundation/Foundation.h>

extern NSString * const kFileManagerFilenamePrefix;

@interface FileManager : NSObject

@property (readonly) NSString *homeDirectory;
@property (readonly) NSString *currentFilename;
@property (readonly) NSString *currentFilePath;

+(FileManager*)sharedInstance;

-(void) startNewFile;

-(NSString*) contentsOfFile:(NSString*)filepath;

-(void) appendString:(NSString*)aString toFile:(NSString*)filepath;

-(void) deleteFileAtPath:(NSString*)filepath;
-(void) clearFile:(NSString*)filepath;
-(void) clearHomeDirectory;

-(void) addDictionary:(NSDictionary*)dictionary toJSONArrayInFile:(NSString*)filepath;
-(NSArray*) filesFromDirectory:(NSString*)dirPath;

@end

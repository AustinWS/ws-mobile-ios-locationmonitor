//
//  NotificationManager.h
//  LocationMonitor
//
//  Created by Austin Kelley on 2/7/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationManager : NSObject

+ (NotificationManager*)sharedInstance;

-(void) fireLocalNotificationWithTitle:(NSString*)title body:(NSString*)body;

@end

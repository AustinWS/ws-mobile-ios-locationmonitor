//
//  ViewController.m
//  LocationMonitor
//
//  Created by Austin Kelley on 2/7/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import "ViewController.h"
#import "LocationManager.h"
#import "NotificationManager.h"
#import "FileManager.h"

#import <MessageUI/MessageUI.h>

@interface ViewController () <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UIButton *updatingLocationButton;
@property (nonatomic, strong) IBOutlet UIButton *monitoringLocationButton;

@property (nonatomic, strong) IBOutlet UILabel  *recentLocationsList;

@end


@implementation ViewController


-(void) viewDidLoad {
    
    [super viewDidLoad];

    [[NotificationManager sharedInstance] fireLocalNotificationWithTitle:@"You killed it!" body:@"That was quick."];
    
    [self updateUpdatingTitle];
    [self updateMonitoringTitle];
    
    [[LocationManager sharedInstance] addObserver:self forKeyPath:kLocationManagerLocationsKey options:0 context:nil];
    [[LocationManager sharedInstance] addObserver:self forKeyPath:kLocationManagerIsUpdatingLocationKey options:0 context:nil];
    [[LocationManager sharedInstance] addObserver:self forKeyPath:kLocationManagerIsMonitoringLocationKey options:0 context:nil];
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (keyPath == kLocationManagerLocationsKey) {
        self.recentLocationsList.text = [[LocationManager sharedInstance] formattedStringFromLocations:[LocationManager sharedInstance].locations];
    } else if (keyPath == kLocationManagerIsUpdatingLocationKey) {
        [self updateUpdatingTitle];
    } else if (keyPath == kLocationManagerIsMonitoringLocationKey) {
        [self updateMonitoringTitle];
    }
}

-(void) updateUpdatingTitle {
    NSString *title = [LocationManager sharedInstance].isUpdatingLocation ? @"Stop Updating Location" : @"Start Updating Location";
    [self.updatingLocationButton setTitle:title forState:UIControlStateNormal];
}

-(void) updateMonitoringTitle {
    NSString *title = [LocationManager sharedInstance].isMonitoringLocation ? @"Stop Monitoring Location" : @"Start Monitoring Location";
    [self.monitoringLocationButton setTitle:title forState:UIControlStateNormal];
}

-(IBAction) toggleUpdatingLocation:(id)sender {
    
    if ([LocationManager sharedInstance].isUpdatingLocation) {
        [[LocationManager sharedInstance] stopUpdatingLocation];
    } else {
        [[LocationManager sharedInstance] startUpdatingLocation];
    }
}

-(IBAction) toggleMonitoringLocation:(id)sender {
    
    if ([LocationManager sharedInstance].isMonitoringLocation) {
        [[LocationManager sharedInstance] stopMonitoringLocation];
    } else {
        [[LocationManager sharedInstance] startMonitoringLocation];
    }
}

-(IBAction) emailLogFile:(id)sender {
    if (![MFMailComposeViewController canSendMail]) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Mail disabled" message:@"This device can't send emails." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    NSArray *files = [[FileManager sharedInstance] filesFromDirectory:nil];
    
    if (files.count == 1) {
        [self sendLogFile:files.firstObject];
        return;
    }
    
    NSInteger substringStart = kFileManagerFilenamePrefix.length;
    files = [files sortedArrayUsingComparator:^NSComparisonResult(NSString * _Nonnull obj1, NSString *  _Nonnull obj2) {
        NSNumber *val1 = @([[obj1 substringFromIndex:substringStart] integerValue]);
        NSNumber *val2 = @([[obj2 substringFromIndex:substringStart] integerValue]);
        return [val1 compare:val2];
    }];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Which log file?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *file1 = [UIAlertAction actionWithTitle:files[files.count-1] style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self sendLogFile:files[files.count-1]];
    }];
    [alert addAction:file1];
    UIAlertAction *file2 = [UIAlertAction actionWithTitle:files[files.count-2] style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self sendLogFile:files[files.count-2]];
    }];
    [alert addAction:file2];
    
    if (files.count >= 3) {
        UIAlertAction *file3 = [UIAlertAction actionWithTitle:files[files.count-3] style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self sendLogFile:files[files.count-3]];
        }];
        [alert addAction:file3];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Nevermind" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) sendLogFile:(NSString*)filename {
    
    NSString *filepath = [[FileManager sharedInstance].homeDirectory stringByAppendingPathComponent:filename];
    NSString *contents = [[FileManager sharedInstance] contentsOfFile:filepath];
    
    MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
    composeVC.mailComposeDelegate = self;
    composeVC.title = @"Send Logs";
    // Configure the fields of the interface.
    [composeVC setSubject:@"LocationMonitor Log File"];
    [composeVC setMessageBody:contents isHTML:NO];
    
    // Present the view controller modally.
    [self presentViewController:composeVC animated:YES completion:nil];
}

-(IBAction) clearLogFile:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Clear log files?" message:@"This cannot be undone." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[FileManager sharedInstance] clearHomeDirectory];
    }];
    [alert addAction:delete];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

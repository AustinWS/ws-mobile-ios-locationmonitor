//
//  LocationManager.h
//  LocationMonitor
//
//  Created by Austin Kelley on 2/7/17.
//  Copyright © 2017 Intersections. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

extern NSString * const kLocationManagerLocationsKey;
extern NSString * const kLocationManagerIsUpdatingLocationKey;
extern NSString * const kLocationManagerIsMonitoringLocationKey;

@interface LocationManager : NSObject

@property (nonatomic, strong) NSArray *locations;

@property (nonatomic) CLLocationAccuracy accuracy;
@property (nonatomic) CLActivityType activityType;

@property (nonatomic, readonly) NSString *accuracyString;
@property (nonatomic, readonly) NSString *activityTypeString;

@property (nonatomic) BOOL isUpdatingLocation;
@property (nonatomic) BOOL isMonitoringLocation;

+ (LocationManager*)sharedInstance;

-(void) startUpdatingLocation;
-(void) stopUpdatingLocation;

-(void) startMonitoringLocation;
-(void) stopMonitoringLocation;

-(NSString*) formattedStringFromLocations:(NSArray*)locations;

@end
